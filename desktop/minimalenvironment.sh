#!/bin/sh

${INSTALLPACKAGE} xorg

# If lightdm
${INSTALLPACKAGE} lightdm-webkit2-greeter
${SUPERUSER} sed -i 's/#greeter-session=example-gtk-gnome/greeter-session=lightdm-webkit2-greeter/g' /etc/lightdm/lightdm.conf
${SUPERUSER} systemctl reenable lightdm
