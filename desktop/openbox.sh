#!/bin/sh

${INSTALLPACKAGE} openbox
${INSTALLPACKAGE} obconf
${INSTALLAUR} obkey

# ${SUPERUSER} sh -c "cat <<EOF > /usr/share/xsessions/stumpwm.desktop
# [Desktop Entry]
# Encoding=UTF-8
# Name=StumpWM
# Comment=Lisp Powered Window Manager
# Exec=/usr/local/bin/stumpwm
# Type=XSession
# EOF"

# Custom Dotfiles Dependencies
${INSTALLPACKAGE} dbus-python # Python bindings for DBUS
${INSTALLPACKAGE} python-prctl # Python(ic) interface to the linux prctl syscall
${INSTALLPACKAGE} dina-font # A monospace bitmap font, primarily aimed at programmers
${INSTALLAUR} python-pywal # Generate and change color-schemes on the fly.
${INSTALLAUR} siji-git # Iconic bitmap font based on stlarch with additional glyphs
${INSTALLAUR} wpgtk-git # A gui wallpaper chooser that changes your Openbox theme, GTK theme and Tint2 theme
${INSTALLPACKAGE} dunst # Customizable and lightweight notification-daemon
${INSTALLPACKAGE} udiskie # Removable disk automounter using udisks
${INSTALLPACKAGE} rofi # A window switcher, application launcher and dmenu replacement
${INSTALLPACKAGE} redshift # Adjusts the color temperature of your screen according to your surroundings.
${INSTALLPACKAGE} scrot # Simple command-line screenshot utility for X
${INSTALLPACKAGE} xcape # Configure modifier keys to act as other keys when pressed and released on their own
${INSTALLPACKAGE} pulseaudio # A featureful, general-purpose sound server
${INSTALLPACKAGE} pulseaudio-alsa # ALSA Configuration for PulseAudio
${INSTALLPACKAGE} polkit-gnome 
${INSTALLAUR} polybar # A fast and easy-to-use status bar

${INSTALLPACKAGE} feh # Fast and light imlib2-based image viewer
${INSTALLPACKAGE} networkmanager # Network connection manager and user applications

${INSTALLPACKAGE} kitty # A modern, hackable, featureful, OpenGL-based terminal emulator
${INSTALLPACKAGE} xarchiver # GTK+ frontend to various command line archivers
${INSTALLPACKAGE} pcmanfm-gtk3 # Extremely fast and lightweight file manager
${INSTALLPACKAGE} lxappearance-gtk3 # Feature-rich GTK+ theme switcher of the LXDE Desktop (GTK+ 3 version)
${INSTALLPACKAGE} picom # X compositor that may fix tearing issues

systemctl --user reenable pulseaudio

# Switch from WPG to Chameleon/Themix
wpg-install.sh -gird

# Discord Pywal Theme
git clone https://github.com/FilipLitwora/pywal-discord ~/.config/pywal-discord
cd ~/.config/pywal-discord/
${SUPERUSER} ./install


# YADM - Yet Another Dotfiles Manager

${INSTALLAUR} yadm 

# yadm clone

yadm clone https://gitlab.com/bloxies-dotfiles/linux-herbstluftwm-dotfiles
