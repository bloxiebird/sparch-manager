#!/bin/sh

${INSTALLPACKAGE} qt5-tools # For qdbus and other QT tools
# Useful qdbus org.kde.KWin /Session quit

${INSTALLPACKAGE} plasma-desktop
${INSTALLPACKAGE} khotkeys
${INSTALLPACKAGE} kde-gtk-config
${INSTALLPACKAGE} breeze-gtk
${INSTALLPACKAGE} sddm
${INSTALLPACKAGE} sddm-kcm
${INSTALLPACKAGE} kde-gtk-config
${INSTALLPACKAGE} kcm-wacomtablet
${INSTALLPACKAGE} plasma-nm
${INSTALLPACKAGE} plasma-pa
${INSTALLPACKAGE} kdeplasma-addons
${INSTALLPACKAGE} dolphin

# ${INSTALLAUR} plasma5-applets-virtual-desktop-bar
# ${INSTALLAUR} plasma5-applets-window-title
# ${INSTALLAUR} plasma5-applets-window-buttons

# ${INSTALLAUR} latte-dock-git

# ${INSTALLAUR} kwin-scripts-krohnkite-git

${INSTALLPACKAGE} scrot
${INSTALLPACKAGE} conky 

# ${SUPERUSER} tee /usr/share/xsessions/kwinx11.desktop <<EOF
# [Desktop Entry]
# Encoding=UTF-8
# Name=KWin Standalone (X11)
# Comment=KWin Standalone for the X11 Windowing System
# Exec=/usr/bin/kwin_x11
# Type=XSession
# EOF

# ${SUPERUSER} tee /usr/share/xsessions/kwinwayland.desktop <<EOF
# [Desktop Entry]
# Encoding=UTF-8
# Name=KWin Standalone (Wayland)
# Comment=KWin Standalone for the Wayland Protocol
# Exec=/usr/bin/kwin_wayland
# Type=XSession
# EOF 

# Set the Current Desktop to KDE, meaning we can set the QT theme and other things via KDE Settings - There must be a better way to set this though!
# #${SUEPRUSER} tee /etc/environment <<EOF
#
# This file is parsed by pam_env module
#
# Syntax: simple "KEY=VAL" pairs on separate lines
#

# XDG_CURRENT_DESKTOP=KDE
# # EOF

# mkdir -p ~/.local/share/kservices5/
# ln -s ~/.local/share/kwin/scripts/krohnkite/metadata.desktop ~/.local/share/kservices5/krohnkite.desktop

# Check if there is battery
if [ `cat /sys/class/power_supply/*` ]; then
    ${INSTALLPACKAGE} powerdevil 
    ${INSTALLPACKAGE} powertop
    sudo powertop --auto-tune
fi

sudo systemctl reenable sddm

# My personal dotfiles
${INSTALLPACKAGE} dbus-python # Python bindings for DBUS
${INSTALLPACKAGE} python-prctl # Python(ic) interface to the linux prctl syscall
# ${INSTALLPACKAGE} dina-font # A monospace bitmap font, primarily aimed at programmers
${INSTALLAUR} pywal-git # Generate and change color-schemes on the fly.
# ${INSTALLAUR} siji-git # Iconic bitmap font based on stlarch with additional glyphs
${INSTALLAUR} yadm

# Pywal Konsole Support
mkdir ~/.cache/wal/
mkdir ~/.local/share/konsole

tee ~/.cache/wal/colors-konsole.colorscheme <<EOF
TEMP
EOF

ln ~/.cache/wal/colors-konsole.colorscheme ~/.local/share/konsole

# KDE Pywal Theme
mkdir ~/.local/share/color-schemes/

tee ~/.cache/wal/KDEPywal.colors <<EOF
TEMP
EOF

ln ~/.cache/wal/KDEPywal.colors ~/.local/share/color-schemes/

# Discord Pywal Theme
git clone https://github.com/FilipLitwora/pywal-discord ~/.config/pywal-discord
cd ~/.config/pywal-discord/
${SUPERUSER} ./install
