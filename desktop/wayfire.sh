#!/bin/sh

${INSTALLAUR} wayfire

${SUPERUSER} tee /etc/environment <<EOF
QT_QPA_PLATFORMTHEME=qt5ct
EOF

# Custom Dotfiles Dependencies
${INSTALLPACKAGE} dbus-python # Python bindings for DBUS
${INSTALLPACKAGE} python-prctl # Python(ic) interface to the linux prctl syscall
${INSTALLPACKAGE} python-pydbus # Pythonic D-Bus library 
# ${INSTALLPACKAGE} dina-font # A monospace bitmap font, primarily aimed at programmers
${INSTALLPACKAGE} ttf-hack # A hand groomed and optically balanced typeface based on Bitstream Vera Mono.
${INSTALLAUR} python-pywal # Generate and change color-schemes on the fly.
# ${INSTALLAUR} siji-git # Iconic bitmap font based on stlarch with additional glyphs
${INSTALLAUR} wpgtk-git # A gui wallpaper chooser that changes your Openbox theme, GTK theme and Tint2 theme
${INSTALLPACKAGE} mako # Lightweight notification daemon for Wayland
${INSTALLPACKAGE} udiskie # Removable disk automounter using udisks
${INSTALLPACKAGE} rofi # A window switcher, application launcher and dmenu replacement

# For pulseaudio, recommended to use pipewire instead
# ${INSTALLPACKAGE} pulseaudio # A featureful, general-purpose sound server
# ${INSTALLPACKAGE} pulseaudio-alsa # ALSA Configuration for PulseAudio
# systemctl --user reenable pulseaudio

# For pipewire
${INSTALLPACKAGE} pipewire # Low-latency audio/video router and processor
${INSTALLPACKAGE} pipewire-docs # Low-latency audio/video router and processor - documentation
${INSTALLPACKAGE} pipewire-media-session # Low-latency audio/video router and processor - Session manager
${INSTALLPACKAGE} pipewire-alsa # Low-latency audio/video router and processor - ALSA configuration
${INSTALLPACKAGE} pipewire-pulse # Low-latency audio/video router and processor - PulseAudio replacement
${INSTALLPACKAGE} pipewire-jack # Low-latency audio/video router and processor - JACK support
${INSTALLPACKAGE} pipewire-zeroconf # Low-latency audio/video router and processor - Zeroconf support
${INSTALLPACKAGE} lib32-pipewire # Low-latency audio/video router and processor - 32-bit client library
${INSTALLPACKAGE} lib32-pipewire-jack # Low-latency audio/video router and processor - 32-bit client library - JACK support

${INSTALLPACKAGE} alsa-utils # Advanced Linux Sound Architecture - Utilities
${INSTALLPACKAGE} polkit-gnome # Legacy polkit authentication agent for GNOME
${INSTALLPACKAGE} xdg-desktop-portal-wlr # xdg-desktop-portal backend for wlroots

${INSTALLPACKAGE} grim # Screenshot utility for Wayland
${INSTALLPACKAGE} slurp # Select a region in a Wayland compositor
${INSTALLPACKAGE} swaybg # Wallpaper tool for Wayland compositors
${INSTALLPACKAGE} swayidle # Idle management daemon for Wayland
${INSTALLPACKAGE} swaylock # Screen locker for Wayland
${INSTALLAUR} wlr-randr # A xrandr clone for wlroots compositors 
${INSTALLAUR} wlsunset # Day/night gamma adjustments for Wayland
${INSTALLPACKAGE} light # Program to easily change brightness on backlight-controllers.

${INSTALLAUR} waybar # Highly customizable Wayland bar for Sway and Wlroots based compositors
${INSTALLAUR} waybar-mpris-git # MPRIS2 waybar component
${INSTALLPACKAGE} otf-font-awesome # Iconic font designed for Bootstrap
${INSTALLPACKAGE} ttf-font-awesome # Iconic font designed for Bootstrap

${INSTALLPACKAGE} networkmanager # Network connection manager and user applications

${INSTALLPACKAGE} kitty # A modern, hackable, featureful, OpenGL-based terminal emulator
# ${INSTALLPACKAGE} xarchiver # GTK+ frontend to various command line archivers
${INSTALLPACKAGE} ark # Archiving Tool
# ${INSTALLPACKAGE} pcmanfm-gtk3 # Extremely fast and lightweight file manager
${INSTALLPACKAGE} dolphin # KDE File Manager
${INSTALLPACKAGE} lxappearance-gtk3 # Feature-rich GTK+ theme switcher of the LXDE Desktop (GTK+ 3 version)
${INSTALLPACKAGE} qt5ct # Qt5 Configuration Utility

${INSTALLPACKAGE} imv # Image viewer for Wayland and X11
# ${INSTALLPACKAGE} feh # Fast and light imlib2-based image viewer


# Install GTK and Icon theme for pywal
wpg-install.sh -gi

# Discord Pywal Theme
git clone https://github.com/FilipLitwora/pywal-discord ~/.config/pywal-discord
cd ~/.config/pywal-discord/
${SUPERUSER} ./install

# YADM - Yet Another Dotfiles Manager

${INSTALLAUR} yadm 

# yadm clone

yadm clone https://gitlab.com/bloxies-dotfiles/linux-wayfire-dotfiles

# Init wayfire
${HOME}/.init.sh 
