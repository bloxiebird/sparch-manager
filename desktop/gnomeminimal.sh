#!/bin/sh

${INSTALLPACKAGE} gnome-shell
${INSTALLPACKAGE} gnome-backgrounds
${INSTALLPACKAGE} gnome-tweaks
${INSTALLPACKAGE} gnome-control-center
${INSTALLPACKAGE} chrome-gnome-shell
${INSTALLPACKAGE} gdm

${INSTALLAUR} gnome-shell-extension-pop-shell-git
${INSTALLAUR} gnome-shell-extension-unite

${INSTALLPACKAGE} nautilus
${INSTALLPACKAGE} file-roller
${INSTALLPACKAGE} feh

${SUPERUSER} reenable gdm

${INSTALLPACKAGE} noto-fonts
${INSTALLPACKAGE} noto-fonts-extra
${INSTALLPACKAGE} noto-fonts-emoji
${INSTALLPACKAGE} noto-fonts-cjk

gsettings set org.gnome.shell.app-switcher current-workspace-only true
