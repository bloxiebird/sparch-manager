#!/bin/sh

# System
${INSTALLPACKAGE} linux-firmware # Firmware files for Linux
${INSTALLPACKAGE} linux # The Linux kernel and modules
${INSTALLPACKAGE} linux-headers # Headers and scripts for building modules for the Linux kernel
${INSTALLPACKAGE} base # Base files
${INSTALLPACKAGE} base-devel # Base development files

# Utilities
${INSTALLPACKAGE} sudo # Sudo
${INSTALLPACKAGE} grub # Grub
${INSTALLPACKAGE} os-prober # OS Prober
${INSTALLPACKAGE} autoconf # A GNU tool for automatically configuring source code
${INSTALLPACKAGE} automake # A GNU tool for automatically creating Makefiles
${INSTALLPACKAGE} binutils # A set of programs to assemble and manipulate binary and object files
${INSTALLPACKAGE} bison # The GNU general-purpose parser generator
${INSTALLPACKAGE} cpio # A tool to copy files into or out of a cpio or tar archive
${INSTALLPACKAGE} dotnet-sdk # The .NET Core SDK
${INSTALLPACKAGE} dosfstools # DOS filesystem utilities
${INSTALLPACKAGE} file # File type identification utility
${INSTALLPACKAGE} findutils # GNU utilities to locate files
${INSTALLPACKAGE} flex # A tool for generating text-scanning programs
${INSTALLPACKAGE} gawk # GNU version of awk
${INSTALLPACKAGE} gettext # GNU internationalization library
${INSTALLPACKAGE} gstreamer # Multimedia graph framework - core
${INSTALLPACKAGE} gst-plugins-good # Multimedia graph framework - good plugins
${INSTALLPACKAGE} gst-plugins-bad # Multimedia graph framework - bad plugins
${INSTALLPACKAGE} gst-plugins-ugly # Multimedia graph framework - ugly plugins
${INSTALLPACKAGE} zip # Compressor/archiver for creating and modifying zipfiles
${INSTALLPACKAGE} wget # Network utility to retrieve files from the Web
${INSTALLPACKAGE} gzip # GNU compression utility
${INSTALLPACKAGE} p7zip # Command-line file archiver with high compression ratio
${INSTALLPACKAGE} unrar # The RAR uncompression program
${INSTALLPACKAGE} unzip # For extracting and viewing files in .zip archives
${INSTALLPACKAGE} sed # GNU stream editor
${INSTALLPACKAGE} pacman # A library-based package manager with dependency support
${INSTALLPACKAGE} libtool # A generic library support script
${INSTALLPACKAGE} efibootmgr # Linux user-space application to modify the EFI Boot Manager
${INSTALLPACKAGE} fakeroot # Tool for simulating superuser privileges
${INSTALLPACKAGE} grep # A string search utility
${INSTALLPACKAGE} ffmpeg # Complete solution to record, convert and stream audio and video
${INSTALLPACKAGE} rlwrap # Adds readline-style editing and history to programs.
${INSTALLPACKAGE} which # A utility to show the full path of commands
${INSTALLPACKAGE} groff # GNU troff text-formatting system
${INSTALLPACKAGE} php # A general-purpose scripting language that is especially suited to web development
${INSTALLPACKAGE} pkgconf # Package compiler and linker metadata toolkit
${INSTALLPACKAGE} make # GNU make utility to maintain groups of programs
${INSTALLPACKAGE} patch # A utility to apply patch files to original sources
${INSTALLPACKAGE} m4 # The GNU macro processor
${INSTALLPACKAGE} gcc # The GNU Compiler Collection - C and C++ frontends
${INSTALLPACKAGE} git # the fast distributed version control system
${INSTALLPACKAGE} rustup # Systems programming language focused on safety, speed and concurrency
rustup toolchain install nightly
${INSTALLPACKAGE} go # Core compiler tools for the Go programming language
${INSTALLPACKAGE} ntfs-3g # NTFS filesystem driver and utilities
${INSTALLPACKAGE} guile # Portable, embeddable Scheme implementation written in C
${INSTALLPACKAGE} texinfo # GNU documentation system for on-line information and printed output
${INSTALLPACKAGE} python
${INSTALLPACKAGE} python-pip # The PyPA recommended tool for installing Python packages

# Applications
${INSTALLPACKAGE} blender # A fully integrated 3D graphics creation suite
${INSTALLPACKAGE} chromium # A web browser built for speed, simplicity, and security
${INSTALLPACKAGE} cmus # Feature-rich ncurses-based music player
# ${INSTALLPACKAGE} discord # All-in-one voice and text chat for gamers that's free and secure.
${INSTALLPACKAGE} emacs # The extensible, customizable, self-documenting real-time display editor
${INSTALLPACKAGE} gimp # GNU Image Manipulation Program
${INSTALLPACKAGE} gnuplot # Plotting package which outputs to X11, PostScript, PNG, GIF, and others
${INSTALLPACKAGE} kdenlive # A non-linear video editor for Linux using the MLT video framework
${INSTALLPACKAGE} mpv # a free, open source, and cross-platform media player
${INSTALLPACKAGE} transmission-gtk # Fast, easy, and free BitTorrent client (GTK+ GUI)
${INSTALLPACKAGE} qutebrowser # A keyboard-driven, vim-like browser based on PyQt5
${INSTALLPACKAGE} ranger # Simple, vim-like file manager
${INSTALLPACKAGE} obs-studio # Free, open source software for live streaming and recording
${INSTALLPACKAGE} steam # Valve's digital software delivery system
${INSTALLPACKAGE} qtpass # A multi-platform GUI for pass
${INSTALLPACKAGE} krita # Edit and paint images
${INSTALLPACKAGE} texlive-most # Most LaTeX files
${INSTALLPACKAGE} kid3-qt # An MP3, Ogg/Vorbis and FLAC tag editor, Qt version
${INSTALLPACKAGE} htop

${INSTALLAUR} pfetch # A pretty system information tool written in POSIX sh.
${INSTALLAUR} freetube-bin # An open source desktop YouTube player built with privacy in mind.

${INSTALLAUR} lightcord-bin # A simple - customizable - Discord Client
${INSTALLAUR} ripcord-arch-libs # Qt-based Discord and Slack client. Modified to run on system libraries for Wayland support.

