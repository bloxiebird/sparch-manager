#!/bin/sh

# Get the current directory (default ~/.sparch/)
SPARCHDIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then SPARCHDIR="$PWD"; fi

# The superuser command to use
SUPERUSER="sudo"

# The pacman install command to use
INSTALLPACKAGE="${SUPERUSER} pacman -S --needed --noconfirm"

# The pacman remove command to use, used only in package tracking
REMOVEPACKAGE="${SUPERUSER} pacman -R --noconfirm"

# The pacman update command to use
UPDATEPACKAGES="${SUPERUSER} pacman -Syu --noconfirm"

# The AUR install command to use
INSTALLAUR="yay -S --needed --noconfirm"

# Enable multilib
${SUPERUSER} sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf

# Make sure to utilise all cores when making files
sudo sed -i 's/#MAKEFLAGS="-j2"/MAKEFLAGS="-j$(nproc)"/g' /etc/makepkg.conf
sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T $(nproc) -z -)/g' /etc/makepkg.conf

${UPDATEPACKAGES}

# Install Microcode
CPUVENDOR=`cat /proc/cpuinfo | grep 'vendor' | uniq | cut -d' ' -f2` 
if [ ${CPUVENDOR} == "AuthenticAMD" ]
then
    ${INSTALLPACKAGE} amd-ucode
else
    ${INSTALLPACKAGE} intel-ucode
fi

# Install GPU Drivers
GPUDRIVER=`lspci | grep VGA | grep -o AMD ||  lspci | grep VGA | grep -o NVIDIA || lspci | grep -o VGA | grep INTEL`
if [ ${GPUDRIVER} == "AMD" ]
then
     ${INSTALLPACKAGE} mesa xf86-video-amdgpu vulkan-radeon libva-mesa-driver mesa-vdpau libva-va-gl

     # Multilib
     ${INSTALLPACKAGE} lib32-mesa lib32-vulkan-radeon lib32-libva-mesa-driver lib32-mesa-vdpau

     # Remove amdvlk just incase as using vulkan-radeon
     ${REMOVEPACKAGE} amdvlk lib32-amdvlk
     
elif [ ${GPUDRIVER} == "NVIDIA" ]
then
     ${INSTALLPACKAGE} nvidia nvidia-utils

    # Multilib
     ${INSTALLPACKAGE} lib32-nvidia-utils
else
     ${INSTALLPACKAGE} mesa xf86-video-intel vulkan-intel libva-intel-driver mesa-vdpau libva-va-gl intel-media-driver

    # Multilib
     ${INSTALLPACKAGE} lib32-mesa lib32-vulkan-intel lib32-libva-intel-driver lib32-mesa-vdpau
fi

# Manage user directories like ~/Desktop and ~/Music
${INSTALLPACKAGE} xdg-user-dirs
xdg-user-dirs-update

# AUR Helper - YAY!
# We make sure the AUR Helper is installed before moving onto any other packages

CONFIRMHELPER=`pacman -Qe yay-git | grep -o yay-git`

if [ -z ${CONFIRMHELPER} ]
then

    # If the AUR helper is already not installed
    ${INSTALLPACKAGE} git base-devel
    git clone https://aur.archlinux.org/yay-git.git ${SPARCHDIR}/outsources/yay-git
    cd ${SPARCHDIR}/outsources/yay-git
    makepkg -si
    cd ${SPARCHDIR}
    
    . "${SPARCHDIR}/config/base.sh"
else

    # Source external shell modules - COMMENT OUT ANYTHING HERE YOU DO NOT NEED
 
    ## Packages
    . "${SPARCHDIR}/packages.sh"

    ## Graphical Environments

    # Minimal KDE Environment
    . "${SPARCHDIR}/desktop/kdeminimal.sh"

    # Minimal GNOME Environment
    # . "${SPARCHDIR}/desktop/gnomeminimal.sh"

    # Openbox
    # . "${SPARCHDIR}/desktop/openbox.sh"
    
    # Herbstluftwm
    #. "${SPARCHDIR}/desktop/herbstluftwm.sh"
    
    # StumpWM
    # . "${SPARCHDIR}/desktop/stumpwm.sh"

    # AwesomeWM
    # . "${SPARCHDIR}/desktop/awesomewm.sh"

    # A minimal xorg/greeter setup for standalone window manager setups
    # . "${SPARCHDIR}/desktop/minimalenvironment.sh"
    
fi



