#!/bin/sh

loadkeys uk

echo "-------------------------------------------------"
echo "Setting up mirrors for optimal download - UK ----"
echo "-------------------------------------------------"
timedatectl set-ntp true
pacman -Sy --noconfirm pacman-contrib
# rm /etc/pacman.d/mirrorlist.backup
# mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
# curl -s "https://www.archlinux.org/mirrorlist/?country=GB&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 - > /etc/pacman.d/mirrorlist



echo "(BOOT PARTITION) Please enter disk: (example /dev/sda1)"
read BOOTDISK
if [ -z ${BOOTDISK} ]
then
    echo "Boot partition cannot be nil"
    exit 1
fi
mkfs.vfat -F32 ${BOOTDISK}

echo "(SWAP PARTITION : IF NO OPTION INPUTTED, DO NOT USE A SWAP PARTITION) Please enter disk: (example /dev/sda2)"
read SWAPDISK
if [ ! -z ${SWAPDISK} ]
then
    mkswap ${SWAPDISK}
    swapon ${SWAPDISK}
fi

echo "(ROOT PARTITION) Please enter disk: (example /dev/sda3)"
read ROOTDISK
if [ -z ${ROOTDISK} ]
then
    echo "Root partition cannot be nil"
    exit 1
fi
mkfs.ext4 ${ROOTDISK}
mount ${ROOTDISK} /mnt

echo "(HOME PARTITION : IF NO OPTION INPUTTED, DO NOT USE A HOME PARTITION) Please enter disk: (example /dev/sda4)"
read HOMEDISK
if [ ! -z ${HOMEDISK} ]
then
    mkfs.ext4 ${HOMEDISK}
    mkdir /mnt/home
    mount ${HOMEDISK} /mnt/home
fi

# Use the pacstrap(8) script to install the base package, Linux kernel and firmware for common hardware:
pacstrap /mnt base base-devel linux linux-headers linux-firmware

# Generate an fstab file (use -U or -L to define by UUID or labels, respectively):
# Check the resulting /mnt/etc/fstab file, and edit it in case of errors.
genfstab -U /mnt >> /mnt/etc/fstab

# Copy chroot install into system
cp installchroot.sh /mnt

# Mount boot now that root is setup
mkdir /mnt/boot
mkdir /mnt/boot/efi

mount ${BOOTDISK} /mnt/boot/efi

# Change root into the new system:
arch-chroot /mnt ./installchroot.sh



