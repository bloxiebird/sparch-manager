#!/bin/sh

echo "(HOSTNAME) Please set hostname: "
read SPARCHHOSTNAME
if [ -z ${SPARCHHOSTNAME} ]
then
    echo "Hostname cannot be nil"
    exit 1
fi

echo "(ROOT USER) Please set password: "
read SPARCHUSERNAME
if [ -z ${SPARCHUSERNAME} ]
then
    echo "Username cannot be nil"
    exit 1
fi

echo "(MAIN ADMINISTRATOR USER) Please set username: "
read SPARCHROOTPASSWORD
if [ -z ${SPARCHROOTPASSWORD} ]
then
    echo "Username cannot be nil"
    exit 1
fi

echo "(MAIN ADMINISTRATOR USER) Please set password: "
read SPARCHUSERPASSWORD
if [ -z ${SPARCHUSERPASSWORD} ]
then
    echo "Username cannot be nil"
    exit 1
fi

# Set the time zone:
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime

# Run hwclock(8) to generate /etc/adjtime:
# This command assumes the hardware clock is set to UTC. See System time#Time standard for details.
hwclock --systohc

# Localization

# Automatically edit the locales
sed -i 's/^#en_GB.UTF-8 UTF-8/en_GB.UTF-8 UTF-8/' /etc/locale.gen
sed -i 's/^#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen

# Edit /etc/locale.gen and uncomment en_US.UTF-8 UTF-8 and other needed locales. Generate the locales by running:
locale-gen

localectl --no-ask-password set-locale LANG=en_GB.UTF-8 LC_COLLATE="" LC_TIME=en_GB.UTF-8

# Set keymaps
localectl --no-ask-password set-keymap uk

# Set keymap
tee /etc/locale.conf <<EOF
LANG=en_GB.UTF-8
EOF

# Hostname
hostnamectl --no-ask-password set-hostname ${SPARCHHOSTNAME}

tee /etc/hostname <<EOF
${SPARCHHOSTNAME}
EOF

# Add matching entries to hosts(5):
tee /etc/hosts <<EOF
127.0.0.1	localhost
::1		localhost
127.0.1.1	${SPARCHHOSTNAME}.localdomain	     ${SPARCHHOSTNAME}
EOF

# Add sudo no password rights
sed -i 's/^# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers

# Install GRUB Bootloader
sudo pacman -Sy --needed --noconfirm efibootmgr grub os-prober
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB

# Use the grub-mkconfig tool to generate /boot/grub/grub.cfg:
grub-mkconfig -o /boot/grub/grub.cfg

# Initramfs
# Creating a new initramfs is usually not required, because mkinitcpio was run on installation of the kernel package with pacstrap.
# For LVM, system encryption or RAID, modify mkinitcpio.conf(5) and recreate the initramfs image:
mkinitcpio -P

chpasswd <<EOF
root:${SPARCHROOTPASSWORD}
EOF

useradd ${SPARCHUSERNAME}

usermod -aG wheel ${SPARCHUSERNAME}

mkhomedir_helper ${SPARCHUSERNAME}

chpasswd <<EOF
${SPARCHUSERNAME}:${SPARCHUSERPASSWORD}
EOF


# Enable Networking
sudo pacman -Sy --needed --noconfirm networkmanager
sudo systemctl enable NetworkManager

# Source the base
# . config/base.sh 
